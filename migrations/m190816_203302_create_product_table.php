<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`, `{{%orders}}`, `{{%pages}}`.
 */
class m190816_203302_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable ('products', [
            'id' => $this->primaryKey (),
            'title' => $this->string(200),
            'alias' => $this->string (200),
            'price' => $this->decimal(6,2),
            'description' =>$this->text ()
        ]);

        $this->createTable ('orders', [
            'id' => $this->primaryKey (),
            'customer_name' => $this->string(200),
            'email' => $this->string(200),
            'phone' => $this->string(200),
            'feedback' =>$this->text ()
        ]);

        $this->createTable ('pages', [
            'id' => $this->primaryKey (),
            'title' => $this->string(200),
            'alias' => $this->string (200),
            'intro' =>$this->text (),
            'content' =>$this->text (),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%orders}}');
        $this->dropTable('{{%pages}}');
    }
}
