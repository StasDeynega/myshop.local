<?php
/**
 * @var \app\models\Product[] $products
 */

?>
<h1><?=$title?></h1>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <div class="alert alert-danger">
        <?=\Yii::$app->session->getFlash('message')?>
    </div>
<?php endif ?>
<p>
    <a href="/products/create" class="btn btn-success">Add new product</a>
</p>
<?php foreach ($products as $product) : ?>

<div class="col-md-4">
    <h2><?=$product->title?></h2>
    <p><?=$product->price?> грн</p>
    <?=\yii\helpers\Html::a(
        'View',
        '/products/view/' . $product->id,
        ['class' => 'btn btn-primary']) ?>
    <?=\yii\helpers\Html::a(
        'Edit',
        '/products/update/' . $product->id,
        ['class' => 'btn btn-info']) ?>
    <?=\yii\helpers\Html::a(
        'Delete',
        '/products/delete/' . $product->id,
        [
            'class' => 'btn btn-danger',
            'onclick' => 'return confirm("Are you sure?")',
        ]) ?>
</div>

<?php endforeach; ?>