
<h1>Add new product:</h1>
<?php use yii\widgets\ActiveForm;
$form = ActiveForm::begin(); ?>

<?= $form->field($product, 'title') ?>
<?= $form->field($product, 'alias') ?>
<?= $form->field($product, 'price') ?>
<?= $form->field($product, 'description')->textarea (['rows' => 4]) ?>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Create</button>
        <a href="/products/index" class="btn btn-danger">Cancel</a>
    </div>

<?php ActiveForm::end(); ?>
