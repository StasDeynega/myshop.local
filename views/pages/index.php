<?php
/**
 * @var \app\models\Page[] $pages
 */

?>
<h1><?=$title?></h1>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <div class="alert alert-danger">
        <?=\Yii::$app->session->getFlash('message')?>
    </div>
<?php endif ?>
<p>
    <a href="/pages/create" class="btn btn-success">Create page</a>
</p>
<?php foreach ($pages as $page) : ?>

    <div class="col-md-4">
        <h2><?=$page->title?></h2>
        <p><?=$page->intro?></p>
        <?=\yii\helpers\Html::a(
            'View',
            '/pages/view/' . $page->id,
            ['class' => 'btn btn-primary'])
        ?>
        <?=\yii\helpers\Html::a(
            'Edit',
            '/pages/update/' . $page->id,
            ['class' => 'btn btn-info']) ?>
        <?=\yii\helpers\Html::a(
            'Delete',
            '/pages/delete/' . $page->id,
            [
                'class' => 'btn btn-danger',
                'onclick' => 'return confirm("Are you sure?")',
            ]) ?>
    </div>

<?php endforeach; ?>
