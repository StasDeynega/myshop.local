<h1>Add new page:</h1>
<?php use yii\widgets\ActiveForm;
$form = ActiveForm::begin(); ?>

<?= $form->field($page, 'title') ?>
<?= $form->field($page, 'alias') ?>
<?= $form->field($page, 'intro') ?>
<?= $form->field($page, 'content')->textarea (['rows' => 4]) ?>

<div class="form-group">
    <button type="submit" class="btn btn-success">Create</button>
    <a href="/pages/index" class="btn btn-danger">Cancel</a>
</div>

<?php ActiveForm::end(); ?>
