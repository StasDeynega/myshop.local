<h1>Edit order:</h1>
<?php use yii\widgets\ActiveForm;
$form = ActiveForm::begin(); ?>

<?= $form->field($order, 'customer_name') ?>
<?= $form->field($order, 'email') ?>
<?= $form->field($order, 'phone') ?>
<?= $form->field($order, 'feedback')->textarea (['rows' => 4]) ?>

<div class="form-group">
    <button type="submit" class="btn btn-success">Save</button>
    <a href="/orders/index" class="btn btn-danger">Cancel</a>
</div>

<?php ActiveForm::end(); ?>
