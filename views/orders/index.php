<?php
/**
 * @var \app\models\Order[] $orders
 */

?>
<h1><?=$title?></h1>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <div class="alert alert-danger">
        <?=\Yii::$app->session->getFlash('message')?>
    </div>
<?php endif ?>
    <p>
        <a href="/orders/create" class="btn btn-success">Enter order</a>
    </p>
    <?php foreach ($orders as $order) : ?>

    <div class="col-md-4">
        <h2><?=$order->customer_name?></h2>
        <p><?=$order->email?></p>
        <p><?=$order->phone?></p>
        <p><?=$order->feedback?></p>
        <?=\yii\helpers\Html::a(
            'Edit',
            '/orders/update/' . $order->id,
            ['class' => 'btn btn-info']) ?>
        <?=\yii\helpers\Html::a(
            'Delete',
            '/orders/delete/' . $order->id,
            [
                'class' => 'btn btn-danger',
                'onclick' => 'return confirm("Are you sure?")',
            ]) ?>
    </div>

    <?php endforeach; ?>