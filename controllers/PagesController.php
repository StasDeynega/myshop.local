<?php


namespace app\controllers;


use app\models\Page;
use Yii;
use yii\web\Controller;

class PagesController extends Controller
{
    public function actionIndex() {

        $pages = Page::find()->all();

        return $this->render (
            'index',
            [
                'title' => 'Info',
                'pages' => $pages,
            ]);
    }

    public function actionView($id)
    {
        $pages = Page::findOne($id);

        return $this->render(
            'view',
            [
                'pages' => $pages,
            ] );
    }

    public function actionCreate()
    {
        $page = new Page();
        if (\Yii::$app->request->isPost) {

            $page->load(\Yii::$app->request->post());
            if ($page->save ()) {
                $this->redirect ('/pages/index');
            }
        }
        return $this->render('create', ['page' => $page]);
    }

    public function actionUpdate($id)
    {
        $page = Page::findOne($id);
        if (Yii::$app->request->isPost) {

            $page->load ( Yii::$app->request->post () );
            if ($page->save ()) {
                $this->redirect ('/pages/index');
            }
        }
        return $this->render('update', ['page' => $page]);
    }

    public function actionDelete($id)
    {
        $page = Page::findOne($id);
        $page->delete();
        \Yii::$app->session->setFlash('message', 'Page ID#' . $id . ' deleted');
        $this->redirect('/pages/index');
    }

}