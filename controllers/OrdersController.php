<?php


namespace app\controllers;


use app\models\Order;
use Yii;
use yii\web\Controller;

class OrdersController extends  Controller
{
    public function actionIndex() {

        $orders = Order::find()->all();

        return $this->render (
            'index',
            [
                'title' => 'Orders List',
                'orders' => $orders,
            ]);
    }

    public function actionCreate()
    {
        $order = new Order();
        if (\Yii::$app->request->isPost) {

            $order->load(\Yii::$app->request->post());
            if ($order->save ()) {
                $this->redirect ('/orders/index');
            }
        }
        return $this->render('create', ['order' => $order]);
    }

    public function actionUpdate($id)
    {
        $order = Order::findOne($id);
        if (Yii::$app->request->isPost) {

            $order->load ( Yii::$app->request->post () );
            if ($order->save ()) {
                $this->redirect ('/orders/index');
            }
        }
        return $this->render('update', ['order' => $order]);
    }

    public function actionDelete($id)
    {
        $order = Order::findOne($id);
        $order->delete();
        \Yii::$app->session->setFlash('message', 'Order ID#' . $id . ' deleted');
        $this->redirect('/orders/index');
    }

}