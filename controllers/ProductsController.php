<?php


namespace app\controllers;


use app\models\Product;
use Yii;
use yii\web\Controller;

class ProductsController extends Controller
{

    public function actionIndex() {

        $products = Product::find()->all();

        return $this->render (
            'index',
            [
            'title' => 'Products List',
            'products' => $products,
            ]);
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);

        return $this->render(
            'view',
            [
            'products' => $product,
            ] );
    }

    public function actionCreate()
    {
        $product = new Product();
        if (Yii::$app->request->isPost) {

            $product->load( Yii::$app->request->post());
            if ($product->save ()) {
                $this->redirect ('/products/index');
            }
        }
        return $this->render('create', ['product' => $product]);
    }

    public function actionUpdate($id)
    {
        $product = Product::findOne($id);
        if (Yii::$app->request->isPost) {

            $product->load ( Yii::$app->request->post () );
            if ($product->save ()) {
                $this->redirect ('/products/index');
            }
        }
        return $this->render('update', ['product' => $product]);
    }

    public function actionDelete($id)
    {
        $product = Product::findOne($id);
        $product->delete();
        \Yii::$app->session->setFlash('message', 'Product ID#' . $id . ' deleted');
        $this->redirect('/products/index');
    }

}